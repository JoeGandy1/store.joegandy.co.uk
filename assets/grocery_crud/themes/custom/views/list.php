<table cellpadding="0" cellspacing="0" border="0" class="table table-th-block table-dark " id="<?php echo uniqid(); ?>">
	<thead class="the-box dark full">
		<tr>
			<?php foreach($columns as $column){?>
				<th><?php echo $column->display_as; ?></th>
			<?php }?>
			<?php if(!$unset_delete || !$unset_edit || !$unset_read || !empty($actions)){?>
			<th class='actions'><?php echo $this->l('list_actions'); ?></th>
			<?php }?>
		</tr>
	</thead>
	<tbody>
		<?php foreach($list as $num_row => $row){ ?>
		<tr id='row-<?php echo $num_row?>'>
			<?php foreach($columns as $column){?>
				<td><?php echo $row->{$column->field_name}?></td>
			<?php }?>
			<?php if(!$unset_delete || !$unset_edit || !$unset_read || !empty($actions)){?>
			<td class='actions'>
				<?php
				if(!empty($row->action_urls)){
					foreach($row->action_urls as $action_unique_id => $action_url){
						$action = $actions[$action_unique_id];
				?>
						<a href="<?php echo $action_url; ?>" class="btn btn-primary btn-perspective btn-sm" role="button">
							<i class="fa fa-eye <?php echo $action->css_class; ?> <?php echo $action_unique_id;?>"></i><?php echo($action->label);?>
						</a>
				<?php }
				}
				?>
				<?php if(!$unset_read){?>
					<a href="<?php echo $row->read_url?>" class="btn btn-info btn-perspective btn-sm" role="button">
						<i class="fa fa-eye"></i>
					</a>
				<?php }?>

				<?php if(!$unset_edit){?>
					<a href="<?php echo $row->edit_url?>" class="btn btn-info btn-perspective btn-sm" role="button">
						<i class="fa fa-pencil"></i>
					</a>
				<?php }?>
				<?php if(!$unset_delete){?>
					<a onclick = "javascript: return delete_row('<?php echo $row->delete_url?>', '<?php echo $num_row?>')"
						href="javascript:void(0)" class="btn btn-danger btn-perspective btn-sm" role="button">
						<i class="fa fa-trash-o"></i>
					</a>
				<?php }?>
			</td>
			<?php }?>
		</tr>
		<?php }?>
	</tbody>
	<!--
	<tfoot>
		<tr>
			<?php foreach($columns as $column){?>
				<th><input type="text" name="<?php echo $column->field_name; ?>" placeholder="<?php echo $this->l('list_search').' '.$column->display_as; ?>" class="search_<?php echo $column->field_name; ?>" /></th>
			<?php }?>
			<?php if(!$unset_delete || !$unset_edit || !$unset_read || !empty($actions)){?>
				<th>
					<button class="btn btn-info btn-perspective btn-sm" role="button" data-url="<?php echo $ajax_list_url; ?>">
						<i class="fa fa-refresh"></i> Refresh			
					</button>
					<a href="javascript:void(0)" role="button" class="btn btn-info btn-perspective btn-sm">
						<i class="fa fa-search-minus"></i> Clear Filter
					</a>
				</th>
			<?php }?>
		</tr>
	</tfoot>
-->
</table>
