<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('ion_auth');
		$this->load->library('form_validation');
		$this->load->helper('url');

		$this->load->library('merchant');
		$this->merchant->load('paypal_express');

		$settings = $this->merchant->default_settings();

		$settings = array(
		    'username' => 'joe_api1.gandy.ws',
		    'password' => '2XFC4U9MGRDC494C',
		    'signature' => 'AOlumExP5XQ1ttUAcZn7sYwSslJ9A2pZTc-QkNYg45hlbTPiT5sGgwOB',
		    'test_mode' => false);

		$this->merchant->initialize($settings);

		// Load MongoDB library instead of native db driver if required
		$this->config->item('use_mongodb', 'ion_auth') ?
		$this->load->library('mongo_db') :

		$this->load->database();

		$this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));

		$this->lang->load('auth');
		$this->load->helper('language');

		$this->load->model('product_model', 'product');
		error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
        error_reporting(-1);
        ini_set('display_errors', 'On');
	}

	protected $debug = true;

	public function index()
	{
		$data['products'] = $this->product->get_products();
		
		$this->load->view('home', $data);
	}

	public function cancelled(){
		echo "Cancelled Purchase";
	}

	public function test($name){
		$this->load->view($name);
	}

	public function thank_you(){
		$user_id = $this->ion_auth->user()->row()->id;

		$latest_order = $this->product->get_latest_order($user_id);

		$params = array(
		    'amount' => $latest_order->price,
		    'currency' => $latest_order->currency
		);
		
		$response = $this->merchant->purchase_return($params);

		if ($response->success()){
		    $this->product->complete_order($user_id);
			$this->load->view('thank_you');
		}
		else{
			$data['error'] = $response->message;
			$this->load->view('error_thank_you', $data);
		}
	}

	public function buy($product_id){
		if (!$this->ion_auth->logged_in())
		{
			//redirect them to the login page
			redirect('auth/login', 'refresh');
		}

		if(!$product_id){
			die('No product id!');
		}

		$user_id = $this->ion_auth->user()->row()->id;

		$product = $this->product->order_product(1, $product_id, $user_id);

		$params = array(
		    'amount' => $product->price,
		    'currency' => $product->currency,
		    'return_url' => 'http://store.joegandy.co.uk/home/thank_you',
		    'cancel_url' => 'http://store.joegandy.co.uk/home/cancelled');

		$response = $this->merchant->purchase($params);
	}


}