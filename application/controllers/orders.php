<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Orders extends CI_Controller {
	protected $data;

	function __construct()
	{
		parent::__construct();
		$this->load->library('ion_auth');
		$this->load->library('form_validation');
		$this->load->helper('url');

		// Load MongoDB library instead of native db driver if required
		$this->config->item('use_mongodb', 'ion_auth') ?
		$this->load->library('mongo_db') :
        $this->load->library('grocery_CRUD');

		$this->load->database();

		$this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));

		$this->lang->load('auth');
		$this->load->helper('language');

		if (!$this->ion_auth->logged_in())
		{
			//redirect them to the login page
			redirect('auth/login', 'refresh');
		}

		$this->load->model('product_model', 'product');
		error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
	}

	public function index(){
		if (!$this->ion_auth->logged_in())
		{
			//redirect them to the login page
			redirect('/auth/login', 'refresh');
		}

		$this->grocery_crud->set_table('sale_log')
			 ->set_theme('custom')
			 ->columns('product_id', 'date', 'url')
			 ->where('user_id', $this->ion_auth->user()->row()->id)
			 ->where('payed', 1)
			 ->set_relation('product_id','products','name')
			 ->display_as('product_id', 'Product')
			 ->display_as('url', 'Product Description URL')
			 ->callback_column('url',array($this,'_get_dl_url'))
			 ->add_action('Versions', '', '','',array($this,'_product_versions'))
			 //->add_action('Download Latest Version', '', '','ui-icon-down',array($this,'_product_versions'))
			 ->unset_add()
			 ->unset_edit()
			 ->unset_delete()
			 ->unset_read();
        
        $this->data = $this->grocery_crud->render();

		$this->load->view('orders/orders', $this->data);
	}

	public function _get_dl_url($value, $row)
	{
		$product = $this->product->get_product($row->product_id);
	  	return '<a href="'.$product->url.'">'.$product->url.'</a>';
	}

	public function _product_versions($primary_key , $row)
	{
	    return 'orders/versions/'.$row->product_id;
	}

	public function _add_link($value, $row)
	{
	  	return '<a href="'.$row->download_url.'">Click here to download</a>';
	}

	public function versions($id=false)
	{

		if (!$this->ion_auth->logged_in())
		{
			//redirect them to the login page
			redirect('/auth/login', 'refresh');
		}

		if(!$id)
			redirect('/orders', 'refresh');

		$this->grocery_crud->set_table('product_versions')
			 ->columns('version', 'download_url')
			 ->set_theme('custom')
			 ->unset_add()
			 ->unset_edit()
			 ->unset_delete()
			 ->unset_read()
			 ->callback_column('download_url',array($this,'_add_link'))
			 ->where('product_id',$id);
        
        $this->data = $this->grocery_crud->render();
		$this->load->view('orders/product_versions', $this->data);

	}
}