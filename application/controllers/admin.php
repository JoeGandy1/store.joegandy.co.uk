<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends CI_Controller {
	protected $data;

	function __construct()
	{
		parent::__construct();
		$this->load->library('ion_auth');
		$this->load->library('form_validation');
		$this->load->helper('url');

		// Load MongoDB library instead of native db driver if required
		$this->config->item('use_mongodb', 'ion_auth') ?
		$this->load->library('mongo_db') :
        $this->load->library('grocery_CRUD');

		$this->load->database();

		$this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));

		$this->lang->load('auth');
		$this->load->helper('language');

		$this->load->model('statistic_model', 'stat');
		$this->load->model('product_model', 'product');
		error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
	}

	public function index()
	{

		if (!$this->ion_auth->logged_in())
		{
			//redirect them to the login page
			redirect('admin/login', 'refresh');
		}
		elseif (!$this->ion_auth->is_admin()) //remove this elseif if you want to enable this for non-admins
		{
			//redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}
		$this->data['customers'] = $this->stat->count_rows('users');
		$this->data['products'] = $this->stat->count_rows('products');
		$this->data['todays_sales'] = $this->stat->todays_sales();
		$this->data['yesterdays_sales'] = $this->stat->yesterdays_sales();
		$this->data['years_earnings'] = $this->stat->year_earnings();
		$this->data['most_recent_sales'] = $this->stat->most_recent_sales(5);
		//die(print_r($this->data['most_recent_sales']));

		$this->load->view('admin/home', $this->data);
	}

	//log the user in
	function login()
	{
		if ($this->ion_auth->logged_in())
		{
			//redirect them to the login page
			redirect('admin', 'refresh');
		}

		//validate form input
		$this->form_validation->set_rules('identity', 'Identity', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');

		if ($this->form_validation->run() == true)
		{
			$remember = (bool) $this->input->post('remember');

			if ($this->ion_auth->login($this->input->post('identity'), $this->input->post('password'), $remember))
			{
				$this->session->set_flashdata('message', $this->ion_auth->messages());
				redirect('/admin', 'refresh');
			}
			else
			{
				$this->session->set_flashdata('message', $this->ion_auth->errors());
				redirect('admin/login', 'refresh');
			}
		}
		else
		{
			//the user is not logging in so display the login page
			//set the flash data error message if there is one
			$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

			$this->data['identity'] = array('name' => 'identity',
				'id' => 'identity',
				'type' => 'text',
				'class'        => 'form-control no-border input-lg rounded',
				'placeholder'  => 'Enter email',
				'value' => $this->form_validation->set_value('identity'),
			);
			$this->data['password'] = array('name' => 'password',
				'class' => 'form-control no-border input-lg rounded',
				'placeholder' => 'Enter Password',
				'id' => 'password',
				'type' => 'password',
			);

			$this->_render_page('admin/login', $this->data);
		}
	}

	function logout()
	{
		$logout = $this->ion_auth->logout();

		$this->session->set_flashdata('message', $this->ion_auth->messages());
		redirect('admin/login', 'refresh');
	}

	public function users()
	{

		if (!$this->ion_auth->logged_in())
		{
			//redirect them to the login page
			redirect('admin/login', 'refresh');
		}
		elseif (!$this->ion_auth->is_admin()) //remove this elseif if you want to enable this for non-admins
		{
			//redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}

		$this->grocery_crud->set_table('users')
			 ->columns('first_name','last_name','email','company')
			 ->add_fields('first_name', 'last_name', 'email', 'company', 'password', 'phone', 'active')
			 ->set_theme('custom')
			->callback_insert(array($this, 'create_user_callback'));;
        
        $this->data = $this->grocery_crud->render();
		$this->load->view('admin/users', $this->data);
	}

	function create_user_callback($post_array, $primary_key = null) {

		$username = $post_array['first_name'] . ' ' . $post_array['last_name'];
		$password = $post_array['password'];
		$email = $post_array['email'];
		$data = array(
		'phone' => $post_array['phone'],
		'first_name' => $post_array['first_name'],
		'last_name' => $post_array['last_name'],
		'company' => $post_array['company']
		);
		$this->ion_auth_model->register($username, $password, $email, $data);

		return $this->db->insert_id();
	}

	public function groups()
	{

		if (!$this->ion_auth->logged_in())
		{
			//redirect them to the login page
			redirect('admin/login', 'refresh');
		}
		elseif (!$this->ion_auth->is_admin()) //remove this elseif if you want to enable this for non-admins
		{
			//redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}

		$this->grocery_crud->set_table('groups')
			 ->set_theme('custom');
        
        $this->data = $this->grocery_crud->render();
		$this->load->view('admin/groups', $this->data);
	}

	public function products()
	{

		if (!$this->ion_auth->logged_in())
		{
			//redirect them to the login page
			redirect('admin/login', 'refresh');
		}
		elseif (!$this->ion_auth->is_admin()) //remove this elseif if you want to enable this for non-admins
		{
			//redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}

		$this->grocery_crud->set_table('products')
			 ->columns('name', 'price', 'currency', 'desc', 'type')
			 ->set_theme('custom')
			 ->add_action('Versions', '', 'admin/versions','ui-icon-plus');
        
        $this->data = $this->grocery_crud->render();
		$this->load->view('admin/products', $this->data);
	}

	public function versions($id=false)
	{

		if (!$this->ion_auth->logged_in())
		{
			//redirect them to the login page
			redirect('admin/login', 'refresh');
		}
		elseif (!$this->ion_auth->is_admin()) //remove this elseif if you want to enable this for non-admins
		{
			//redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}
		if(!$id)
			redirect('admin/products', 'refresh');

		$this->grocery_crud->set_table('product_versions')
			 ->columns('version', 'download_url')
			 ->set_theme('custom')
			 ->unset_read()
    		 ->callback_after_insert(array($this, 'new_version'))
			 ->where('product_id',$id);
        
        $this->data = $this->grocery_crud->render();
		$this->load->view('admin/product_versions', $this->data);

	}

	public function new_version($post_array=null, $primary_key=null)
	{
		$version = $post_array['version'];
		$download_url = $post_array['download_url'];
		$product = $post_array['product_id'];

		$config = Array(
		    'mailtype'  => 'html', 
		    'charset'   => 'iso-8859-1'
		);

		$text = "";
		$name = "";

		$buyers = $this->product->get_buyers($product);

	  	$this->load->library('email');
	  	$this->email->initialize($config);
	  	$enabled = false;//While still in dev dont send emails
	  	if($enabled){
			foreach($buyers as $buyer){
				$name = $buyer['name'];
				$text = "Dear ".$buyer['email'].",<br><br>";
				$text .= "Your plugin, '".$buyer['name']."' you've purchased has been updated<br>";
				$text .= "To download your plugin simple <a href='".$download_url."'>Click here</a><br>";
				$text .= "This product is now on version ".$version."<br>";
				$text .= "<br>";
				$text .= "If you have any other questions, don't hesitate to contact Joe using the email 'Joe@gandy.ws'<br>";
				$text .= "Or sending him a message from the original site where you found this product<br>";
				$text .= "Download all versions of your plugin at <a href='http://store.joe.lc/'>store.joe.lc</a><br>";
				$text .= "<br>";
				$text .= "Kind Regards,<br>";
				$text .= "Joe Gandy<br>";

				$this->email->from('updates@joe.lc');
				$this->email->to($buyer['email']); 

				$this->email->subject($name.' has been updated');
				$this->email->message($text);

				$this->email->send();
			}
		}

		//mail('joe@gandy.ws', 'debug', implode(" - ", $post_array));

	//	echo 'done';
	}
/*
	public function email_users(){

		$config = Array(
		    'mailtype'  => 'html', 
		    'charset'   => 'iso-8859-1'
		);

	  	$this->load->library('email');
	  	$this->email->initialize($config);

		$text = "paypal@revlak.com,customer4359,rcplilley85@gmail.com,customer1357,alrfool911@yahoo.com,customer8693,payment@sinnLoser.com,customer0927,billing@skywatergaming.net,customer1263,cwhite@whiteservermc.tk,customer4860,samforbis@gmail.com,customer3120,idk.lah@gmail.com,customer9409,tobiaserdmann19@gmail.com,customer2840,doku24hd@gmail.com,customer1810,omartin17@hotmail.com,customer0028,youare@sticed.de,customer9301,pmflav@tpg.com.au,customer2787,info@scapp.net,customer6376,arc_crook@hotmail.com,customer8746,tagban@gmail.com,customer3546,nightcore86@hotmail.fr,customer2849,tansie@gmail.com,customer1290,dBo007@gmx.de,customer8534,appletrombone@gmail.com,customer0829,davespanzer@hotmail.com,customer9203,mobiusgamersstaff@gmail.com,customer6782,lem0petr@gmail.com,customer0121,jakegriffinbackup@gmail.com,customer2536,support@exonmedia.ca,customer4728,tommo1224@live.co.uk,customer8172,ecko@gamerzwithattitude.net,customer8029,admin@subzeronetwork.com,customer0928,contact@piedmontmc.net,customer5264,ryladine@gahservers.us,customer2385,admin@sangaming.com,customer3432,mishkasmirnow@gmail.com,customer1293,nexuz@live.no,customer1038,andrewzah@outlook.com,customer1832,noreply@nice-hosting.dk,customer9102,alex@necessarygaming.com,customer8283,donate@xenogamers.org,customer1929,kanv1s@yandex.ru,customer9389,c4f117@gmail.com,customer1284,techrunner@gmx.com,customer7364,dimension7623@att.net,customer9273,lars.jorgen@me.com,customer0394,community@jackofdesigns.com,customer0247,henderson.charles.r@gmail.com,customer7462,erwinj@telenet.be,customer4523";
		$arr = explode(",", $text);
		for($x = 0; $x < count($arr); $x+=2){
			echo $arr[$x]." ".$arr[$x+1]."<br>";
			$this->email->from('updates@joe.lc');
			$this->email->to($arr[$x]); 

			$this->email->subject('Plugin management update');

			$text = "Hi, I've updated the way in which i handle my plugin updates<br>";
			$text .= "If you are no longer interested in my plugins, please ignore this email...<br><br>";
			$text .= "I've created a store (<a href='http://store.joe.lc/'>http://store.joe.lc/</a>), from here you can download older versions of the plugin and also gives me better control of version releases and security of downloads<br>";
			$text .= "I've set up an account for to login into this new site, login details below: <br><br>";
			$text .= "Login-Email: ".$arr[$x]."<br>";
			$text .= "Login-Pass:  ".$arr[$x+1]."<br><br>";
			$text .= "Thanks for your time,<br>";
			$text .= "Joe Gandy";

			$this->email->message($text);
			$this->email->send();
		}

	}
*/
	public function sales()
	{
		if (!$this->ion_auth->logged_in())
		{
			//redirect them to the login page
			redirect('admin/login', 'refresh');
		}
		elseif (!$this->ion_auth->is_admin()) //remove this elseif if you want to enable this for non-admins
		{
			//redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}

		$this->grocery_crud->set_table('sale_log')
			 ->set_theme('custom')
			 //->unset_edit()
			 ->unset_delete()
			 ->unset_read();
        
        $this->data = $this->grocery_crud->render();
		$this->load->view('admin/sales', $this->data);
	}

	function _render_page($view, $data=null, $render=false)
	{

		$this->viewdata = (empty($data)) ? $this->data: $data;

		$view_html = $this->load->view($view, $this->viewdata, $render);

		if (!$render) return $view_html;
	}
}