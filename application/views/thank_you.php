<?php include('includes/header.php'); ?>
		<div class="wrapper">
			<!-- BEGIN TOP NAV -->
			<div class="top-navbar">
				<div class="top-navbar-inner">
					<div class="logo-brand">
					</div>
					<?php include('includes/navbar.php'); ?>
				</div>
			</div>
			<!-- END TOP NAV -->
				
			<div class="container-fluid"><br><br>
						<div class="the-box no-border store-list">
							 <h3>Thanks for you purchase, it has been completed successfully.</h3>
							 <h5>To view all of your orders as well as download your product visit: <a href="/orders">http://store.joe.lc/orders</a></h5>
						</div><!-- /.the-box no-border -->
				</div>
					
<?php include('includes/footer.php'); ?>