<?php include('includes/header.php'); ?>
		<div class="login-header text-center">
			<img src="/assets/img/logo-login.png" class="logo" alt="Logo">
		</div>
		<div class="login-wrapper">
			<?php if($message != ""){?>
			<div class="alert alert-info alert-block fade in alert-dismissable">
			  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			  <strong>There has been an error:</strong><br> <?php echo $message;?></a>
			</div>
			<?php }?>
			
			<?php echo form_open("admin/login");?>
				
				<div class="form-group has-feedback lg left-feedback no-label">
			      <?php echo form_input($identity);?>
				  <span class="fa fa-user form-control-feedback"></span>
				</div>

				<div class="form-group has-feedback lg left-feedback no-label">
			    <?php echo form_input($password);?>
				  <span class="fa fa-unlock-alt form-control-feedback"></span>
				</div>

				<div class="form-group">
				  <div class="checkbox">
					<label>
					  <input type="checkbox" class="i-yellow-flat"> Remember me
					  <?php echo form_checkbox('remember', '1', FALSE, 'id="remember"');?>
					</label>
				  </div>
				</div>
				<div class="form-group">
					<?php $submit = array('name' => 'submit',
						'type' => 'submit',
						'class'        => 'btn btn-warning btn-lg btn-perspective btn-block',
						'value' => 'Login',
					);?>
					<?php echo form_submit($submit);?>
				</div>

			<?php echo form_close();?>
		</div>
<?php include('includes/footer.php'); ?>