		
<?php if(($this->uri->segment(2) != 'login') && ($this->uri->segment(2) != 'register')){?>
		</div><!-- /.container-fluid -->		
		<footer>
			© 2014 <a href="http://www.thebitmotion.com/">TheBitMotion</a><br>
		</footer>	
	</div><!-- /.page-content -->
</div>
<?php }?>

<!-- MAIN JAVASRCIPT (REQUIRED ALL PAGE)-->
<script src="/assets/js/jquery.min.js"></script>
<script src="/assets/js/bootstrap.min.js"></script>
<script src="/assets/plugins/retina/retina.min.js"></script>
<script src="/assets/plugins/nicescroll/jquery.nicescroll.js"></script>
<script src="/assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="/assets/plugins/backstretch/jquery.backstretch.min.js"></script>
		 
		<!-- PLUGINS -->
<script src="/assets/plugins/skycons/skycons.js"></script>
<script src="/assets/plugins/prettify/prettify.js"></script>
<script src="/assets/plugins/magnific-popup/jquery.magnific-popup.min.js"></script>
<script src="/assets/plugins/owl-carousel/owl.carousel.min.js"></script>
<script src="/assets/plugins/chosen/chosen.jquery.min.js"></script>
<script src="/assets/plugins/icheck/icheck.min.js"></script>
<script src="/assets/plugins/datepicker/bootstrap-datepicker.js"></script>
<script src="/assets/plugins/timepicker/bootstrap-timepicker.js"></script>
<script src="/assets/plugins/mask/jquery.mask.min.js"></script>
<script src="/assets/plugins/validator/bootstrapValidator.min.js"></script>
<script src="/assets/plugins/datatable/js/jquery.dataTables.min.js"></script>
<script src="/assets/plugins/datatable/js/bootstrap.datatable.js"></script>
<script src="/assets/plugins/summernote/summernote.min.js"></script>
<script src="/assets/plugins/markdown/markdown.js"></script>
<script src="/assets/plugins/markdown/to-markdown.js"></script>
<script src="/assets/plugins/markdown/bootstrap-markdown.js"></script>
<script src="/assets/plugins/slider/bootstrap-slider.js"></script>
		 
		<!-- MAIN APPS JS -->
<script src="/assets/js/apps.js"></script>
	</body>
</html>