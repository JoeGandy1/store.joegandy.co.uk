<!-- BEGIN SIDEBAR RIGHT HEADING -->
<div class="sidebar-right-heading">
	<ul class="nav nav-tabs square nav-justified">
	  <li class="active"><a href="#setting-sidebar" data-toggle="tab"><i class="fa fa-cogs"></i></a></li>
	</ul>
</div><!-- /.sidebar-right-heading -->
<!-- END SIDEBAR RIGHT HEADING -->



<!-- BEGIN SIDEBAR RIGHT -->
<div class="sidebar-right sidebar-nicescroller" tabindex="5001" style="overflow: hidden; outline: none;">
	<div class="tab-content">
	  <div class="tab-pane fade in active" id="setting-sidebar">
		<ul class="sidebar-menu setting-sidebar">
			<li class="static">ACCOUNT SETTING</li>
			<li class="text-content">
				<div class="switch">
					<div class="onoffswitch blank">
						<input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="onlinestatus" checked="">
						<label class="onoffswitch-label" for="onlinestatus">
							<span class="onoffswitch-inner"></span>
							<span class="onoffswitch-switch"></span>
						</label>
					</div>
				</div>
				Online status
			</li>
			<li class="text-content">
				<div class="switch">
					<div class="onoffswitch blank">
						<input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="offlinecontact" checked="">
						<label class="onoffswitch-label" for="offlinecontact">
							<span class="onoffswitch-inner"></span>
							<span class="onoffswitch-switch"></span>
						</label>
					</div>
				</div>
				Show offline contact
			</li>
			<li class="text-content">
				<div class="switch">
					<div class="onoffswitch blank">
						<input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="invisiblemode">
						<label class="onoffswitch-label" for="invisiblemode">
							<span class="onoffswitch-inner"></span>
							<span class="onoffswitch-switch"></span>
						</label>
					</div>
				</div>
				Invisible mode
			</li>
			<li class="text-content">
				<div class="switch">
					<div class="onoffswitch blank">
						<input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="personalstatus" checked="">
						<label class="onoffswitch-label" for="personalstatus">
							<span class="onoffswitch-inner"></span>
							<span class="onoffswitch-switch"></span>
						</label>
					</div>
				</div>
				Show my personal status
			</li>
			<li class="text-content">
				<div class="switch">
					<div class="onoffswitch blank">
						<input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="deviceicon">
						<label class="onoffswitch-label" for="deviceicon">
							<span class="onoffswitch-inner"></span>
							<span class="onoffswitch-switch"></span>
						</label>
					</div>
				</div>
				Show my device icon
			</li>
			<li class="text-content">
				<div class="switch">
					<div class="onoffswitch blank">
						<input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="logmessages">
						<label class="onoffswitch-label" for="logmessages">
							<span class="onoffswitch-inner"></span>
							<span class="onoffswitch-switch"></span>
						</label>
					</div>
				</div>
				Log all message
			</li>
		</ul>
	  </div><!-- /#setting-sidebar -->
	</div><!-- /.tab-content -->
</div><!-- /.sidebar-right -->
<!-- END SIDEBAR RIGHT -->