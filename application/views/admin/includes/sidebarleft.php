<!-- BEGIN SIDEBAR LEFT -->
<div class="sidebar-left sidebar-nicescroller" tabindex="5000" style="overflow: hidden; outline: none;">
	<ul class="sidebar-menu">
		<li class="static left-profile-summary">
						<div class="media">
						  <p class="pull-left">
							<img src="/assets/img/avatar/avatar.jpg" class="avatar img-circle media-object" alt="Avatar">
						  </p>
						  <div class="media-body">
							<h4>Welcome back,<br> <strong><?php echo $this->ion_auth->user()->row()->first_name.' '.$this->ion_auth->user()->row()->last_name; ?></strong></h4>
							<button onclick="location.href = '/admin/logout';"class="btn btn-danger btn-xs">Log out</button>
						  </div>
						</div>
					</li>
		<li><a href="/admin"><i class="fa fa-dashboard icon-sidebar"></i>Dashboard</a></li>
		<li><a href="/admin/users"><i class="fa fa-user icon-sidebar"></i>Users</a></li>
		<li><a href="/admin/groups"><i class="fa fa-users icon-sidebar"></i>Groups</a></li>
		<li><a href="/admin/products"><i class="fa fa-money icon-sidebar"></i>Products</a></li>
		<li><a href="/admin/sales"><i class="fa fa-bar-chart-o icon-sidebar"></i>Sales</a></li>
	</ul>
</div><!-- /.sidebar-left -->
<!-- END SIDEBAR LEFT -->