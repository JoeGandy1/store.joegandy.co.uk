<?php include('includes/header.php'); ?>
		<div class="wrapper">
			<!-- BEGIN TOP NAV -->
			<div class="top-navbar">
				<div class="top-navbar-inner">
					<div class="logo-brand" style="padding-top:12px;">
						Joe Gandy - Store
					</div>
					<?php include('includes/navbar.php'); ?>
				</div>
			</div>
			<!-- END TOP NAV -->
			
			<?php include('includes/sidebarleft.php');?>

			<?php include('includes/sidebarright.php');?>
			
			<!-- BEGIN PAGE CONTENT -->
			<div class="page-content">
				
				
				<div class="container-fluid">
					<!-- Begin page heading -->
					<h1 class="page-heading">Admin control panel <small>Dashboard</small></h1>
					<!-- End page heading -->
				
					
					<div class="alert alert-info alert-bold-border fade in alert-dismissable">
					  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					  <p><strong>Welcome!</strong></p>
					  <p class="text-muted">You are looking at the administration section for the joe.lc webstore (see the main site <a href="/">here</a>)</p>
					</div>
					<div class="row">
					<div class="col-sm-12">
							<div class="the-box">
								<h4 class="small-title">This Year's Sales Earnings</h4>
								<div id="sales-chart" style="height: 250px;"></div>
							</div>
						</div>
					</div>
					<script type="text/javascript">

						Morris.Area({
						  element: 'sales-chart',
						  data: [
						  <?php
						$months = array();
					 	$months[1] = 'January';
					 	$months[2] = 'February';
					 	$months[3] = 'March';
					 	$months[4] = 'April';
					 	$months[5] = 'May';
					 	$months[6] = 'June';
					 	$months[7] = 'July';
					 	$months[8] = 'August';
					 	$months[9] = 'September';
					 	$months[10] = 'October';
					 	$months[11] = 'November';
					 	$months[12] = 'December';

					  	for($x = 1; $x < 13; $x++){
						  		$found = false;
						  		foreach($years_earnings as $data){
						  			if($data['month'] == $x){
						  				$found = true;
						  				echo '{ y: \''.$months[$x].'\', a: '.$data['amount'].'},'."\n";
						  			}
						  		}
						  		if(!$found){
						  			echo '{ y: \''.$months[$x].'\', a: 0},'."\n";
						  		}
						  }?>],
						  parseTime: false,
						  xkey: 'y',
						  ykeys: ['a'],
						  labels: ['Amount (USD)'],
						  resize: true,
						  lineColors: ['#3BAFDA']
						});
					</script>
					<div class="row">
						<div class="col-sm-4">
							<div class="the-box no-border bg-success tiles-information">
								<i class="fa fa-shopping-cart icon-bg"></i>
								<div class="tiles-inner text-center">
									<p>Today's Sales</p>
									<h1 class="bolded"><?php echo $todays_sales;?></h1> 
								</div><!-- /.tiles-inner -->
							</div><!-- /.the-box no-border -->
						</div><!-- /.col-sm-3 -->
						<div class="col-sm-4">
							<div class="the-box no-border bg-primary tiles-information">
								<i class="fa fa-shopping-cart icon-bg"></i>
								<div class="tiles-inner text-center">
									<p>Yesterday's Sales</p>
									<h1 class="bolded"><?php echo $yesterdays_sales;?></h1> 
								</div><!-- /.tiles-inner -->
							</div><!-- /.the-box no-border -->
						</div><!-- /.col-sm-3 -->
						<div class="col-sm-4">
							<div class="the-box no-border bg-danger tiles-information">
								<i class="fa fa-money icon-bg"></i>
								<div class="tiles-inner text-center">
									<p>No. of products</p>
									<h1 class="bolded"><?php echo $products;?></h1> 
								</div><!-- /.tiles-inner -->
							</div><!-- /.the-box no-border -->
						</div><!-- /.col-sm-3 -->
						<div class="col-sm-4">
							<div class="the-box no-border bg-warning tiles-information">
								<i class="fa fa-users icon-bg"></i>
								<div class="tiles-inner text-center">
									<p>No. of customers</p>
									<h1 class="bolded"><?php echo $customers;?></h1> 
								</div><!-- /.tiles-inner -->
							</div><!-- /.the-box no-border -->
						</div><!-- /.col-sm-3 -->
						<div class="col-sm-4">
							<div class="the-box no-border bg-info tiles-information">
								<i class="fa fa-money icon-bg"></i>
								<div class="tiles-inner text-center">
									<p>Total Income (USD)</p>
									<h1 class="bolded"><?php echo $customers*5;?></h1>
								</div><!-- /.tiles-inner -->
							</div><!-- /.the-box no-border -->
						</div><!-- /.col-sm-3 -->
						<div class="col-sm-4">
							<div class="the-box no-border bg-dark tiles-information">
								<i class="fa fa-money icon-bg"></i>
								<div class="tiles-inner text-center">
									<p>More stats soon</p>
									<h1 class="bolded">.</h1>
								</div><!-- /.tiles-inner -->
							</div><!-- /.the-box no-border -->
						</div><!-- /.col-sm-3 -->
					</div>
					<div class="the-box no-border">
								<h4 class="small-heading more-margin-bottom">RECENT SALES</h4>
								<ul class="media-list media-xs">
								  <?php foreach($most_recent_sales as $sale){?>
								  	  <?php
								  	  	$days = floor((time() - strtotime($sale['date'])) / 86400);
								  	  	?>
									  <li class="media">
										<a class="pull-left" href="#fakelink">
										  <img class="media-object img-circle" src="assets/img/avatar/avatar.jpg" alt="Avatar">
										</a>
										<div class="media-body">
										  <h4 class="media-heading"><a href="#fakelink"><?php echo $sale['username'] ?></a> has purchased <a href="#fakelink"><?php echo $sale['name']; ?></a></h4>
										  <p class="date"><small><?php echo $days; ?> DAYS AGO</small></p>
										  <p class="small">
										  	<a href="#fakelink">Stentor Server List</a> was purchased on <a href="#"><?php echo $sale['date']; ?></a>, for $<?php echo $sale['price']; ?><br>It was purchased by <a href="#fakelink"><?php echo $sale['username']; ?></a>, using the email address <?php echo $sale['email']; ?>
										  </p>
										</div>
									  </li>
								  <?php }?>
								</ul>
							</div><!-- /.the-box no-border -->
<?php include('includes/footer.php'); ?>