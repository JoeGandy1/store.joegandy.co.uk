<?php include('includes/header.php'); ?>
		<div class="wrapper">
			<!-- BEGIN TOP NAV -->
			<div class="top-navbar">
				<div class="top-navbar-inner">
					<div class="logo-brand" style="padding-top:12px;">
						Joe Gandy - Store
					</div>
					<?php include('includes/navbar.php'); ?>
				</div>
			</div>
			<!-- END TOP NAV -->
				
			<div class="container-fluid">
					<!-- Begin page heading -->
					<h1 class="page-heading">Product list <small></small></h1>
					<!-- End page heading -->
				
					<!-- Begin breadcrumb -->
					<ol class="breadcrumb default square rsaquo sm">
						<li><a href="/"><i class="fa fa-home"></i></a></li>
						<li><a href="#">Store</a></li>
						<li class="active">Product list</li>
					</ol>
					<!-- End breadcrumb -->
					
					<?php foreach($products as $product){?>
						<div class="the-box no-border store-list">
							 <div class="media">
	                            <a class="pull-left" href="/home/buy/<?php echo $product['id']; ?>">
	                            	<?php if($product['preview_img']){?>
	                            		<img alt="image" class="store-image img-responsive" src="<?php echo $product['preview_img']?>">
	                            	<?php } else { ?>
	                            		<img alt="image" class="store-image img-responsive" src="assets/img/photo/medium/img.jpg">
	                            	<?php } ?>
	                        	</a>
	                            <div class="clearfix visible-xs"></div>
	                            <div class="media-body">
	                                <h4 class="media-heading">
	                                  <a href=""><strong><?php echo $product['name'];?></strong></a>
	                                </h4>
	                                <ul class="list-inline">
	                                    <li>Developed By <a href="http://www.joe.lc/">Joe Gandy</a></li>
	                                    <li style="list-style: none">|</li>
	                                    <li><span class="label label-danger"><?php echo $product['type']; ?></span></li>
	                                </ul>
	                                <p class="hidden-xs">
										<?php echo $product['desc']; ?>
									</p>
									<a href="/home/buy/<?php echo $product['id']; ?>"><button class="btn btn-info active">Buy This <?php echo $product['type']; ?></button></a>
	                            </div><!-- /.media-body -->
	                        </div><!-- /.media -->
						</div><!-- /.the-box no-border -->
					<?php }?>
					
				
				</div>
					
<?php include('includes/footer.php'); ?>