<div class="top-nav-content">

	<div style="padding:10px;display:inline-block">
		<a href="http://www.joe.lc/"><button class="btn btn-success btn-perspective">Visit my main site</button></a>
	</div>
	
	<div class="pull-right" style="padding:10px;">
		<?php if ($this->ion_auth->logged_in()) {?>
			<?php if ($this->ion_auth->is_admin()) { ?>
				<a href="/admin"><button class="btn btn-warning btn-perspective">Admin</button></a>
			<?php }?>
			<a href="/"><button class="btn btn-primary btn-perspective">Back to Home</button></a>
			<a href="/auth/logout"><button class="btn btn-danger btn-perspective">Logout</button></a>
		<?php }else{ ?>
			<a href="/auth/login"><button class="btn btn-primary btn-perspective">Login</button></a>
			<a href="/auth/register"><button class="btn btn-danger btn-perspective">Register</button></a>
		<?php }?>
	</div><!-- /.btn-collapse-sidebar-right -->
</div><!-- /.top-nav-content -->