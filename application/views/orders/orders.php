<?php include('includes/header.php'); ?>
		<div class="wrapper">
			<!-- BEGIN TOP NAV -->
			<div class="top-navbar">
				<div class="top-navbar-inner">
					<div class="logo-brand" style="padding-top:12px;">
						Joe Gandy - Store
					</div>
					<?php include('includes/navbar.php'); ?>
				</div>
			</div>
			<!-- END TOP NAV -->
			
			<!-- BEGIN PAGE CONTENT -->
			<div class="page-content" style="margin-left:0;">
				
				<div class="container-fluid">
					<!-- Begin page heading -->
					<h1 class="page-heading">My Orders <small><?php echo $this->ion_auth->user()->row()->email; ?></small></h1>
					<!-- End page heading -->
				
					<!-- Begin breadcrumb -->
					<ol class="breadcrumb default square rsaquo sm">
						<li><a href="/"><i class="fa fa-home"></i></a></li>
						<?php
						if($this->uri->segment(1)){
							echo '<li>'.ucfirst($this->uri->segment(1)).'</li>';
						}
						if($this->uri->segment(2)){
							echo '<li>'.ucfirst($this->uri->segment(2)).'</li>';
						}
						if($this->uri->segment(3)){
							echo '<li>'.ucfirst($this->uri->segment(3)).'</li>';
						}?>
					</ol>
					<!-- End breadcrumb -->
					
					<!-- BEGIN DATA TABLE -->
					<?php echo $output; ?>
					<!-- END DATA TABLE -->
					
<?php include('includes/footer.php'); ?>