<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8"> 
	<link rel="stylesheet" type="text/css" href="/assets/css/main.css">
	<title>Joe.lc - Store</title>
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>
	<link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">
    <!--
	Will stall the page until its all loaded
    <script src="js/animateCSS.min.js"></script>
    <script src="js/pace.min.js"></script>
	-->

	<!-- BOOTSTRAP CSS (REQUIRED ALL PAGE)-->
	<link href="/assets/css/bootstrap.min.css" rel="stylesheet">
	 
	<!-- PLUGINS CSS -->
	<!--
	<link href="assets/plugins/weather-icon/css/weather-icons.min.css" rel="stylesheet">
	<link href="assets/plugins/prettify/prettify.min.css" rel="stylesheet">
	<link href="assets/plugins/magnific-popup/magnific-popup.min.css" rel="stylesheet">
	<link href="assets/plugins/owl-carousel/owl.carousel.min.css" rel="stylesheet">
	<link href="assets/plugins/owl-carousel/owl.theme.min.css" rel="stylesheet">
	<link href="assets/plugins/owl-carousel/owl.transitions.min.css" rel="stylesheet">
	<link href="assets/plugins/chosen/chosen.min.css" rel="stylesheet">
	<link href="assets/plugins/icheck/skins/all.css" rel="stylesheet">
	<link href="assets/plugins/datepicker/datepicker.min.css" rel="stylesheet">
	<link href="assets/plugins/timepicker/bootstrap-timepicker.min.css" rel="stylesheet">
	<link href="assets/plugins/validator/bootstrapValidator.min.css" rel="stylesheet">
	<link href="assets/plugins/summernote/summernote.min.css" rel="stylesheet">
	<link href="assets/plugins/markdown/bootstrap-markdown.min.css" rel="stylesheet">
	<link href="assets/plugins/datatable/css/bootstrap.datatable.min.css" rel="stylesheet">
	<link href="assets/plugins/slider/slider.min.css" rel="stylesheet">
	 -->
	 
	<!-- MAIN CSS (REQUIRED ALL PAGE)-->
	<link href="/assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet">
	<link href="/assets/css/style.css" rel="stylesheet">
	<link href="/assets/css/style-responsive.css" rel="stylesheet">
</head>
<body>