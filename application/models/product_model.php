<?php 
class Product_model extends CI_Model {

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    function get_product($id){
      $query = "SELECT * FROM products WHERE products.id = $id";
      $query = $this->db->query($query);
      return  $query->first_row();
    }

    function get_products($amount=false){
    	$amount_qry = $amount ? "LIMIT $amount" : "";

    	$query = $this->db->query(
    	   "SELECT * FROM products
    	    $amount_qry");
   		$result = $query->result_array();
   		//die(print_r($result));
   		return $result;

    }

    function get_buyers($id){      
      $query = "SELECT user_id, email, name
                FROM sale_log 
                INNER JOIN products 
                ON products.id = sale_log.product_id 
                INNER JOIN users on users.id = sale_log.user_id
                WHERE product_id = $id 
                AND payed = 1";
      $query = $this->db->query($query);
      return  $query->result_array();
    }

    function order_product($quantity, $product_id, $user_id){
        $query = "SELECT * FROM products WHERE products.id = $product_id;";

        $query = $this->db->query($query);

        $row = $query->first_row();

        $today = getDate();
        $today = sprintf("%02d",$today['mon']-1).'/'.sprintf("%02d",$today['mday']).'/'.$today['year'];
        
        $query2 = "INSERT INTO sale_log (`product_id`,`user_id`,`date`,`payed`) VALUES ('".$product_id."','".$user_id."',CURDATE(),'0');";

        $this->db->query($query2);

        return $row;
    }

    function get_latest_order($user_id){
        $query_str = "SELECT * FROM sale_log
                          INNER JOIN users 
                          ON users.id = sale_log.user_id
                          INNER JOIN products
                          ON products.id = sale_log.product_id
                      WHERE sale_log.user_id = $user_id
                      ORDER BY sale_log.id DESC
                      LIMIT 1;";

        $query = $this->db->query($query_str);

        $row = $query->first_row();

        return $row;
    }

    function complete_order($user_id){
        $query_str = "SELECT * FROM sale_log
                      WHERE sale_log.user_id = $user_id
                      ORDER BY sale_log.id DESC
                      LIMIT 1;";

        $query = $this->db->query($query_str);
        $row = $query->first_row();

        $order_id = $row->id;

        $query_str = "UPDATE sale_log
                      SET payed=1
                      WHERE sale_log.id = $order_id;";

        $this->db->query($query_str);
    }
}