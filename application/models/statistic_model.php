<?php 
class Statistic_model extends CI_Model {

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    function count_rows($table){
    	$query = $this->db->query("SELECT COUNT(*) AS count FROM $table");
   		$row = $query->row();
   		return $row->count;
    }

    function todays_sales(){
    	$today = getDate();
    	$today = sprintf("%02d",$today['mon']).'/'.sprintf("%02d",$today['mday']).'/'.$today['year'];
    	$query = $this->db->query("SELECT COUNT(*) AS count FROM sale_log WHERE date = $today AND payed = 1");
   		$row = $query->row();
   		return $row->count;
    }

    function yesterdays_sales(){
    	$today = getDate();
    	$today = sprintf("%02d",(($today['mon'])-1)).'/'.sprintf("%02d",$today['mday']).'/'.$today['year'];
    	$query = $this->db->query("SELECT COUNT(*) AS count FROM sale_log WHERE date = $today AND payed = 1");
   		$row = $query->row();
   		return $row->count;
    }

    function year_earnings(){
    	$today = getDate();
    	$year = $today['year'];
    	$month = $today['mon'];

    	$query = $this->db->query(
    	   "SELECT EXTRACT(MONTH FROM date) AS month,
    			  (
              (SELECT price FROM products AS p 
              WHERE p.id = products.id)
            *
              (SELECT COUNT(*) FROM sale_log
              WHERE EXTRACT(MONTH FROM s.date) = EXTRACT(MONTH FROM sale_log.date)
              AND payed = 1)
            )
            AS amount
    		FROM sale_log AS s
    		INNER JOIN products ON products.id = s.product_id
    		WHERE (EXTRACT(YEAR FROM date)) = $year
        AND payed = 1
    		GROUP BY (EXTRACT(MONTH FROM date))");
   		$result = $query->result_array();
   		//die(print_r($result));
   		return $result;
    }

    function most_recent_sales($amount){
      $query = $this->db->query(
         "SELECT users.first_name,
                 users.last_name,
                 users.email,
                 users.username,
                 sale_log.date,
                 sale_log.product_id,
                 products.name,
                 products.price
          FROM sale_log
          INNER JOIN users on users.id = sale_log.user_id
          INNER JOIN products on products.id = sale_log.product_id
          WHERE payed = 1
          ORDER BY date DESC
          LIMIT $amount");
      $result = $query->result_array();
      //die(print_r($result));
      return $result;

    }

}